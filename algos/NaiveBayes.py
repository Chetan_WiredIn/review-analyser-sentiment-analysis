import pickle
import nltk
import random

# ('Original Naive Bayes Algo accuracy percent:', 78.11961703320915)

root = "../"

print "Loading training data"
train_feature_sets_f = open(root + "pickled/imdb/train_feature_sets_500.pickle","rb")
train_features = pickle.load(train_feature_sets_f)
train_feature_sets_f.close()

#print train_features[1]

print "Loading test data"
test_feature_sets_f = open(root + "pickled/imdb/test_feature_sets_500.pickle","rb")
test_features = pickle.load(test_feature_sets_f)
test_feature_sets_f.close()

#print test_features[1]
random.shuffle(train_features)
random.shuffle(test_features)

print "Loading classifier"
naivebayes_f = open(root + "pickled/imdb/originalnaivebayes_500.pickle","rb")
classifier = pickle.load(naivebayes_f)
naivebayes_f.close()

print "Training data!"
classifier = nltk.NaiveBayesClassifier.train(train_features)
print "Testing data"
print("Original Naive Bayes Algo accuracy percent:", (nltk.classify.accuracy(classifier, test_features))*100)
classifier.show_most_informative_features(15)

'''save_classifier = open(root + "pickled/imdb/originalnaivebayes5k.pickle","wb")
pickle.dump(classifier, save_classifier)
save_classifier.close()'''