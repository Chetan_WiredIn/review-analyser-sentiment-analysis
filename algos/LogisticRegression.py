import pickle
import nltk
import random
from sklearn.linear_model import LogisticRegression
from nltk.classify.scikitlearn import SklearnClassifier

# ('Logistic Regression accuracy percent:', 79.109081440532)
root = "../"

print "Loading training data"
train_feature_sets_f = open(root + "pickled/imdb/train_feature_sets_500.pickle","rb")
train_features = pickle.load(train_feature_sets_f)
train_feature_sets_f.close()

#print train_features[1]

print "Loading test data"
test_feature_sets_f = open(root + "pickled/imdb/test_feature_sets_500.pickle","rb")
test_features = pickle.load(test_feature_sets_f)
test_feature_sets_f.close()

#print test_features[1]
random.shuffle(train_features)
random.shuffle(test_features)

print "Training data"
LogisticRegression = SklearnClassifier(LogisticRegression())
LogisticRegression.train(train_features)
print "Testing data"
print("Logistic Regression accuracy percent:", (nltk.classify.accuracy(LogisticRegression, test_features))*100)

save_classifier = open(root + "pickled/imdb/Logistic_Regression_500.pickle","wb")
pickle.dump(LogisticRegression, save_classifier)
save_classifier.close()