import pickle
import nltk
import random
from sklearn import tree
from nltk.classify.scikitlearn import SklearnClassifier

#('Decision Tree accuracy percent:', 67.48788206545687)

root = "../"

print "Loading training data"
train_feature_sets_f = open(root + "pickled/imdb/train_feature_sets_500.pickle","rb")
train_features = pickle.load(train_feature_sets_f)
train_feature_sets_f.close()

#print train_features[1]

print "Loading test data"
test_feature_sets_f = open(root + "pickled/imdb/test_feature_sets_500.pickle","rb")
test_features = pickle.load(test_feature_sets_f)
test_feature_sets_f.close()

#print test_features[1]
random.shuffle(train_features)
random.shuffle(test_features)

print "Training data"
dtree = SklearnClassifier(tree.DecisionTreeClassifier())
dtree.train(train_features)
print "Testing data"
print("Decision Tree accuracy percent:", (nltk.classify.accuracy(dtree, test_features))*100)

save_classifier = open(root + "pickled/imdb/Decision_tree_500.pickle","wb")
pickle.dump(dtree, save_classifier)
save_classifier.close()