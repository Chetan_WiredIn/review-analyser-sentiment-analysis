import pickle
import nltk
import random
from sklearn.svm import SVC, LinearSVC, NuSVC
from nltk.classify.scikitlearn import SklearnClassifier

####################################################################################
## ('LinearSVC_classifier accuracy percent:', 78.91279093057726) - 500 words #######
## ('SVC_classifier accuracy percent:', 77.29038977686977) - 500 words #############             
## ('NuSVC_classifier accuracy percent:', 77.2823779193206) - 500 words ############
####################################################################################
root = "../"

print "Loading training data"
train_feature_sets_f = open(root + "pickled/imdb/train_feature_sets_5k.pickle","rb")
train_features = pickle.load(train_feature_sets_f)
train_feature_sets_f.close()

#print train_features[1]

print "Loading test data"
test_feature_sets_f = open(root + "pickled/imdb/test_feature_sets_5k.pickle","rb")
test_features = pickle.load(test_feature_sets_f)
test_feature_sets_f.close()

#print test_features[1]
random.shuffle(train_features)
#random.shuffle(test_features)

print "Training data"
LinearSVC_classifier = SklearnClassifier(LinearSVC())
LinearSVC_classifier.train(train_features)
print "Testing data"
print("LinearSVC_classifier accuracy percent:", (nltk.classify.accuracy(LinearSVC_classifier, test_features))*100)

save_classifier = open(root + "pickled/imdb/LinearSVC_classifier_5k.pickle","wb")
pickle.dump(LinearSVC_classifier, save_classifier)
save_classifier.close()

print "Training data"
SVC_classifier = SklearnClassifier(SVC())
SVC_classifier.train(train_features)
print "Testing data"
print("SVC_classifier accuracy percent:", (nltk.classify.accuracy(SVC_classifier, test_features))*100)

save_classifier = open(root + "pickled/imdb/SVC_classifier_5k.pickle","wb")
pickle.dump(SVC_classifier, save_classifier)
save_classifier.close()

print "Training data"
NuSVC_classifier = SklearnClassifier(SVC())
NuSVC_classifier.train(train_features)
print "Testing data"
print("NuSVC_classifier accuracy percent:", (nltk.classify.accuracy(NuSVC_classifier, test_features))*100)

save_classifier = open(root + "pickled/imdb/NuSVC_classifier_5k.pickle","wb")
pickle.dump(NuSVC_classifier, save_classifier)
save_classifier.close()
