import nltk
import random
import sys
import os
import glob
import pickle
from collections import Counter
from nltk.classify import ClassifierI
from statistics import mode

reload(sys)
sys.setdefaultencoding("utf-8")

############################################################################################################################

class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            #print str(c) + ": " + str(v)
            votes.append(v)
        choice_votes = votes.count(mode(votes))
        conf = choice_votes / len(votes)

        return (mode(votes), conf)

############################################################################################################################

class PreProcess(object):
    def __init__(self):
        self.sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
        self.word_tokenizer = nltk.tokenize.TreebankWordTokenizer()
        self.tagger = nltk.data.load(nltk.tag._POS_TAGGER)
        self.allowed_word_types = ["J"]
        self.all_adjectives = []
        self.word_features = []
        self.negatives = ["not","nor","neither"]

        word_features_f = open("pickled/imdb/word_features_500.pickle","rb")
        self.word_features = pickle.load(word_features_f)
        word_features_f.close()

    def getWords(self,rev):
        try:
            res = []
            sentences = self.sent_tokenizer.tokenize(rev)
            for sentence in sentences:
                words = self.word_tokenizer.tokenize(sentence)
                tagged = self.tagger.tag(words)
                for i in range(len(tagged)):
                    if (tagged[i][1][0] == "J" and tagged[i-1][0] not in self.negatives and \
                        tagged[i-2][0] not in self.negatives):
                        res.append(tagged[i][0].lower())

            return res
        except:
            return False

    def findFeatures(self,rev):
        try:
            words = self.getWords(rev)
            #print words
            features = {}
            for w, c in self.word_features:
                #print w
                features[w] = (w in words)

            return features
        except:
            print "Not built"
            return {}

    def build(self,filename):
        f = open(filename,'r')
        rev = f.read()
        feat = self.findFeatures(rev)
        if bool(feat):
            return feat
        else:
            print "Not working"
            return None

##############################################################################################################################

classifier_f = open("pickled/imdb/originalnaivebayes_500.pickle","rb")
naivebayes = pickle.load(classifier_f)
classifier_f.close()

classifier_f = open("pickled/imdb/LinearSVC_classifier_500.pickle","rb")
linearsvc = pickle.load(classifier_f)
classifier_f.close()

classifier_f = open("pickled/imdb/Logistic_Regression_500.pickle","rb")
logisticregression = pickle.load(classifier_f)
classifier_f.close()

classifier = VoteClassifier(naivebayes,linearsvc,logisticregression)
util = PreProcess()

def getSentiment(rev):
    return classifier.classify(util.build(rev))

#print getSentiment(sys.argv[1])
if len(sys.argv) > 1:
    feat = util.build(sys.argv[1])
    print classifier.classify(feat)
