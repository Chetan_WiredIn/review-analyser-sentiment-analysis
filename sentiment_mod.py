import nltk
import random
import sys
import os
import glob
import pickle
from collections import Counter

reload(sys)
sys.setdefaultencoding("utf-8")

#############################################################################################################

## Initializing all necceessary tools and data structures ##
sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
word_tokenizer = nltk.tokenize.TreebankWordTokenizer()
tagger = nltk.data.load(nltk.tag._POS_TAGGER)

allowed_word_types = ["J"]
all_adjectives = []
word_features = []
trainFeatureSets = []
testFeatureSets = []
negatives = ["not","nor","neither","at"]



#############################################################################################################


def replaceAliases(rev):
	## Method to replace any aliases with appropriate key ##
	try:
		rev = rev.replace("<br />","")
		rev = rev.replace("n't"," not")
		return rev
	except:
		return rev

def preprocess(rev):
	## Method to pre process the review and POS tag them ##
	try:
		res = []
		sentences = sent_tokenizer.tokenize(rev)
		for sentence in sentences:
			tokens = word_tokenizer.tokenize(sentence)
			words = [w for w in tokens if not w in nltk.corpus.stopwords.words('english')]
			" ".join(words)
			tagged = tagger.tag(words)
			res.append(tagged)

		return res
	except:
		return False


def getAdjectives(rev):
	## Method to get BOW of a given para ##
	try:
		res = []
		sentences = sent_tokenizer.tokenize(rev)
		for sentence in sentences:
			words = word_tokenizer.tokenize(sentence)
			tagged = tagger.tag(words)
			for i in range(len(tagged)):
				if (tagged[i][1][0] == "J" and tagged[i-1][0] not in negatives and tagged[i-2][0] not in negatives):
					res.append(tagged[i][0].lower())

		return res
	except:
		return False

def buildSentiWords(tagged):
	## Method to extract adjectives and append it to the mai list ##
	try:
		for sentence in tagged:
			for w in sentence:
				if w[1][0] == "J":
					all_adjectives.append(w[0].lower())
	except:
		print "Error in building Sentiment words"
		pass

def buildWordFeatures():
	## Method to extract top n frequenrt adjectives fro the all adjectives ##
	try:
		print "Building word features"
		c = Counter(all_adjectives)
		for word in c.most_common(5000):
			word_features.append(word)
	except:
		print "\nFeatures not built.\n"
		exit(1)

def getWordFeatures():
	## Method to extract all reviews, preprocess and finally build the word features ## 
	count = 1
	for filename in glob.glob(os.path.join("datasets/aclimdb/train/pos", '*.txt')):
					print "Getting word features: ",count
					count+=1
					f = open(filename,"r")
					rev = f.read()
					buildSentiWords(preprocess(rev))
	count = 1
	for filename in glob.glob(os.path.join("datasets/aclimdb/train/neg", '*.txt')):
					print "Getting word features: ",count
					count+=1
					f = open(filename,"r")
					rev = f.read()
					buildSentiWords(preprocess(rev))
	buildWordFeatures()
	print word_features[:50]
	
	save_word_features = open("pickled/imdb/word_features5k.pickle","wb")
	pickle.dump(word_features, save_word_features)
	save_word_features.close()

def findFeatures(rev):
    ## Build the feature set of an individual review ##
    try:
    	rev = replaceAliases(rev)
        words = getAdjectives(rev)
        #print words
        features = {}
        for w, c in word_features:
        	#print w
        	features[w] = (w in words)

        return features
    except:
    	print "Not built"
        return {}


##############################################################################################################

## Driver code ##

#getWordFeatures()
word_features_f = open("pickled/imdb/word_features5k.pickle","rb")
word_features = pickle.load(word_features_f)
word_features_f.close()
'''
count = 0
for filename in glob.glob(os.path.join("datasets/aclImdb/train/pos", '*.txt')):
	f = open(filename,'r')
	rev = f.read()
	feat = findFeatures(rev)
	if bool(feat) :
		trainFeatureSets.append( (feat, "pos"))
		count+=1
		print "Train pos: " + str(count)
	else:
		print "Not built training data"

count = 0
for filename in glob.glob(os.path.join("datasets/aclImdb/train/neg", '*.txt')):
	f = open(filename,'r')
	rev = f.read()
	feat = findFeatures(rev)
	if bool(feat):
		trainFeatureSets.append( (feat, "neg"))
		count+=1
		print "Train neg: " + str(count)
	else:
		print "Not built training data"

save_train_featureSets = open("pickled/train_feature_sets5k.pickle","wb")
pickle.dump(trainFeatureSets, save_train_featureSets)
save_train_featureSets.close()
'''
count = 0
for filename in glob.glob(os.path.join("datasets/aclImdb/test/pos", '*.txt')):
	f = open(filename,'r')
	rev = f.read()
	feat = findFeatures(rev)
	if bool(feat) :
		testFeatureSets.append( (feat, "pos"))
		count+=1
		print "Test pos: " + str(count)
	else:
		print "Not built training data"

count = 0
for filename in glob.glob(os.path.join("datasets/aclImdb/test/neg", '*.txt')):
	f = open(filename,'r')
	rev = f.read()
	feat = findFeatures(rev)
	if bool(feat):
		testFeatureSets.append( (feat, "neg"))
		count+=1
		print "Test neg: " + str(count)
	else:
		print "Not built testing data"

save_test_featureSets = open("pickled/test_feature_sets5k.pickle","wb")
pickle.dump(testFeatureSets, save_test_featureSets)
save_test_featureSets.close()

print testFeatureSets

exit(1)

